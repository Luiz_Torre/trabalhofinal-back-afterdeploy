class CursesController < ApplicationController
  before_action :set_curse, only: [:show, :update, :destroy]

  # GET /curses
  def index
    @curses = Curse.all

    render json: @curses
  end

  # GET /curses/1
  def show
    render json: @curse
  end

  # POST /curses
  def create
    @curse = Curse.new(curse_params)

    if @curse.save
      render json: @curse, status: :created, location: @curse
    else
      render json: @curse.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /curses/1
  def update
    if @curse.update(curse_params)
      render json: @curse
    else
      render json: @curse.errors, status: :unprocessable_entity
    end
  end

  # DELETE /curses/1
  def destroy
    @curse.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_curse
      @curse = Curse.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def curse_params
      params.require(:curse).permit(:subject_id, :numberofstudent, :coursecoordinator_id, :knowledgearea, :HeadquartersCampus, :name)
    end
end
