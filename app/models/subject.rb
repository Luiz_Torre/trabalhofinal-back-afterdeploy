class Subject < ApplicationRecord
  has_many :curse
  has_many :grades
  has_many :prerequisite
  has_many :schoolclasses
  has_many :students

  validates :name, :knowledgearea, :workload ,presence: true 
  
end
