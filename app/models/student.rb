class Student < ApplicationRecord
    belongs_to :user
    has_many :subjects
    has_many :grades

end
