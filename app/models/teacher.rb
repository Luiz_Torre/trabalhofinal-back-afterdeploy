class Teacher < ApplicationRecord
    belongs_to :user
    has_many :schoolclasses
    has_many :license
    belongs_to :departament
end
