class Department < ApplicationRecord
  has_many :teacher
  belongs_to :departmentcoordinator

  
  validates :name, :knowledgearea, :HeadquartersCampus, presence: true 
  validates :departmentcoordinator_id, uniqueness: true, presence: true

end
