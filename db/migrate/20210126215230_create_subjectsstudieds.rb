class CreateSubjectsstudieds < ActiveRecord::Migration[5.2]
  def change
    create_table :subjectsstudieds do |t|
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
