class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.references :user, null: false, foreign_key: true
      t.references :departament, null: false, foreign_key: true

      t.timestamps
    end
  end
end
